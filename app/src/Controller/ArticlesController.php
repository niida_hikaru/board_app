<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Articles Controller
 *
 *
 * @method \App\Model\Entity\Article[] paginate($object = null, array $settings = [])
 */
class ArticlesController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();
        $this->paginate['limit'] = 3;
        $this->loadComponent('Search.Prg', [
            'actions' => ['index']
        ]);        
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $articles = $this->Articles
                    ->find('search', ['search' => $this->request->query])
                    ->contain(['Users.SocialProfiles']);

        $articles = $this->paginate($articles);
        $this->set(compact('articles'));
        $this->set('articleCnt', $this);
    }

    /**
     * View method
     *
     * @param string|null $id Article id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $article = $this->Articles->get($id, [
            'contain' => ['Users.SocialProfiles', 'Comments', 'Likes']
        ]);

        $this->loadModel('Likes');
        $like = $this->Likes->find()
            ->where(['article_id' => $id])
            ->andWhere(['user_id' => $this->Auth->user('id')])
            ->count();

        $this->set(compact('article', 'like'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());

            $article->user_id = $this->Auth->user('id');

            if ($this->Articles->save($article)) {
                $this->Flash->success(__('記事を保存しました。'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('記事を保存できませんでした。時間を置いて試しみてください。'));
        }
        $this->set(compact('article'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Article id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $article = $this->Articles->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('記事を編集しました。'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('記事を編集できませんでした。時間を置いて試しみてください。'));
        }
        $this->set(compact('article'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Article id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->get($id);

        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('記事を削除しました。'));
        } else {
            $this->Flash->error(__('記事を削除できませんでした。時間を置いて試してみてください。'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function countLikes($articles_id){
        $this->loadModel('Likes');
        $totalLikes = $this->Likes->find()
            ->where(['article_id' => $articles_id])
            ->count();
        
        return $totalLikes;
    }

    public function isAuthorized($user){
        if(in_array($this->request->getParam('action'), ['edit', 'delete'], true)){
            $articleId = (int)$this->request->getParam('pass.0');
            if($this->Articles->isOwnedBy($articleId, $user['id'])){
                return true;
            }
        }

        if(in_array($this->request->getParam('action'), ['add'], true)){
            return true;
        }
        
        return parent::isAuthorized($user);
    }
}
