<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersTable Test Case
 */
class UsersTableTest extends TestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users',
        'app.articles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Users') ? [] : ['className' => UsersTable::class];
        $this->Users = TableRegistry::get('Users', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Users);

        parent::tearDown();
    }

    public function testFindUser(){
        $query = $this->Users->find('all')
            ->select(['id', 'name', 'email', 'password']);
        $this->assertInstanceOf('Cake\ORM\Query', $query);
        $result = $query->enableHydration(false)->toArray();

        $expected = [
            [
                'id' => 1,
                'name' => 'Alex',
                'email' => 'test@test.com', 
                'password' => 'test1234'
            ]
        ];

        $this->assertEquals($expected, $result);
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        //エラー無しの場合
        $user = $this->Users->newEntity([
            'name' => 'Scott',
            'password' => 'test1234'
        ]);
        $expected = [];
        $this->assertSame($expected, $user->getErrors());

        //必須項目が空の場合
        $user = $this->Users->newEntity([
            'name' => '',
            'password' => ''
        ]);
        $expected = [
            'name' => ['_empty' => 'This field cannot be left empty'],
            'password' => ['_empty' => 'This field cannot be left empty'],
        ];
        $this->assertSame($expected, $user->getErrors());

        //文字数が少ない場合
        $lessUser = $this->Users->newEntity([
            'password' => str_repeat('a', 5)
        ]);
        $expected = [
            'password' => ['lengthBetween' => '8~255文字で入力してください'],
        ];
        $this->assertSame($expected, $lessUser->getErrors());
        

        //文字数が多い場合
        $moreUser = $this->Users->newEntity([
            'name' => str_repeat('a', 51),
            'password' => str_repeat('a', 256)
        ]);
        $expected = [
            'name' => ['maxLength' => '50字以内で入力してください'],
            'password' => ['lengthBetween' => '8~255文字で入力してください'],
        ];
        $this->assertSame($expected, $moreUser->getErrors());
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    /*
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }*/
}
