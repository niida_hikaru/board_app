<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ForgotPasswordsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ForgotPasswordsTable Test Case
 */
class ForgotPasswordsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ForgotPasswordsTable
     */
    public $ForgotPasswords;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.forgot_passwords',
        'app.users',
        'app.articles',
        'app.comments',
        'app.social_profiles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ForgotPasswords') ? [] : ['className' => ForgotPasswordsTable::class];
        $this->ForgotPasswords = TableRegistry::get('ForgotPasswords', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ForgotPasswords);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
