<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CommentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CommentsTable Test Case
 */
class CommentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CommentsTable
     */
    public $Comments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.comments',
        'app.articles',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Comments') ? [] : ['className' => CommentsTable::class];
        $this->Comments = TableRegistry::get('Comments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Comments);

        parent::tearDown();
    }


    public function testValidationDefault()
    {
        //エラー無しの場合
        $comment = $this->Comments->newEntity([
            'name' => 'test user',
            'comment' => 'test comment!!'
        ]);
        $expected = [];
        $this->assertSame($expected, $comment->getErrors());

        //必須項目が空の場合
        $comment = $this->Comments->newEntity([
            'name' => '',
            'comment' => ''
        ]);
        $expected = [
            'name' => ['_empty' => 'This field cannot be left empty'],
            'comment' => ['_empty' => 'This field cannot be left empty']
        ];
        $this->assertSame($expected, $comment->getErrors());
    }
}
