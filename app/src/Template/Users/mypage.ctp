<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<h3 class="text-center"><?= __('マイページ') ?></h3><br>

<legend>ユーザー情報</legend>
<div class="users form large-9 medium-8 columns content">
<?= $this->Form->create($user) ?>
<?= $this->Form->control('name', ['label' => 'ユーザー名']) ?>
<?= $this->Form->control('email', ['label' => 'メールアドレス']) ?>
<?= $this->Form->button(__('保存')) ?>
<?= $this->Form->end() ?>
<br>

<legend>投稿した記事一覧</legend>
<pre class="pre-scrollable">
    <ul>
    <?php foreach($ownArticles as $article): ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <h1>Title: <?= h($article->title) ?></h1>
            <p><h1>content:</h1><?= $this->Text->autoParagraph(h($article->content)) ?></p>
        </div>
    </div>
    <?php endforeach; ?>
    </ul>
</pre>
      
</div>
