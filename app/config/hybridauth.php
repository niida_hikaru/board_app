<?php
use Cake\Core\Configure;

return [
    'HybridAuth' => [
        'providers' => [
            'Google' => [
                'enabled' => true,
                'keys' => [
                    'id' => '1083048570476-acgvuhi23d3ibn4i44lah9dpb14g8nrj.apps.googleusercontent.com',
                    'secret' => '7k9pHorSuLn5PLp9yF-M5lP3'
                ]
            ],
            'Twitter' => [
                'enabled' => true,
                'keys' => [
                    'key' => '4KegdQzVI1IIPrns12OuHx5TD',
                    'secret' => 'opz8cVUbnu86OA2REkTZS6OIgob970uuc0WNjlFmspWgbZAFxd'
                ],
                'includeEmail' => true
            ]
        ],
        'debug_mode' => Configure::read('debug'),
        'debug_file' => LOGS . 'hybridauth.log',
    ]
];