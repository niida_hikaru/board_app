<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('パスワードをリセット') ?>
    <?= $this->Form->control('password', ['required' => true, 'autofocus' => true, 'label' => 'パスワード']) ?>
    </fieldset>
 	<?= $this->Form->button(__('送信')) ?>
    <?= $this->Form->end() ?>
</div>