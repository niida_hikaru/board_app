<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Article[]|\Cake\Collection\CollectionInterface $articles
 */
?>
<h3 class="text-center"><?= __('記事一覧') ?></h3><br>

<?= $this->Form->create(null, ['valueSources' => 'query'])?> 
<div class="input-group">
    <?= $this->Form->input('q', ['label' => '', 'placeholder' => 'タイトル、記事内容で検索']) ?>
<span class="input-group-btn">
    <?= $this->Form->button(__('Search'), ['class' => 'btn btn-info']) ?>
</span>
</div>
<?= $this->Form->end() ?>
<br>
<div class="d-flex align-items-center">
    <table class="table table-hover">
        <thead class="thead-light">
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id', ['label' => 'No']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('title', ['label' => 'タイトル']) ?></th>
                <th scope="col">投稿者</th>
                <th scope="col">いいね数</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($articles as $article): ?>
            <tr data-href="<?= $this->Url->build(['controller'=>'articles', 'action'=>'view', $article->id]) ?>">
                <td><?= $this->Number->format($article->id) ?></td>    
                <td><?= h($article->title) ?></td>
                <td><?= h($article->user->name) ?></td>
                <td><?php echo $articleCnt->countLikes($article->id); ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>

    <ul class="pagination">
        <li><?= $this->Paginator->prev('< ') ?></li>
        <li><?= $this->Paginator->numbers() ?></li>
        <li><?= $this->Paginator->next(' >') ?></li>
    </ul>

    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
