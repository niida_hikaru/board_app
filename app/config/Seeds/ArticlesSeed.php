<?php
use Migrations\AbstractSeed;

/**
 * Articles seed.
 */
class ArticlesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '5',
                'title' => 'aaa',
                'content' => 'aaaaa

',
                'user_id' => '5',
                'created' => '2019-03-12 02:45:17',
                'modified' => '2019-03-12 02:45:17',
            ],
            [
                'id' => '8',
                'title' => '初投稿⭐️',
                'content' => 'ああああああああああああ
ああああああああ',
                'user_id' => '3',
                'created' => '2019-03-15 08:55:49',
                'modified' => '2019-03-15 08:55:49',
            ],
            [
                'id' => '10',
                'title' => 'hi',
                'content' => 'hiiii
',
                'user_id' => '14',
                'created' => '2019-03-18 01:45:53',
                'modified' => '2019-03-18 01:45:53',
            ],
        ];

        $table = $this->table('articles');
        $table->insert($data)->save();
    }
}
