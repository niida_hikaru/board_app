<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('style.css') ?>    
    <?= $this->Html->css('bootstrap/bootstrap.css') ?>
    <?= $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js') ?>
    <?= $this->Html->script('bootstrap/bootstrap.js') ?>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>

    <?= $this->element('HTML/navbar') ?>
    
    <?= $this->Flash->render() ?>

    <div class="container">
        <?= $this->fetch('content') ?>
    </div>

    <footer>
    </footer>
    
    <script>
      $(function() {
      　$('tbody tr[data-href]').addClass('clickable').click(function() {
      　　window.location = $(this).attr('data-href');
      　}).find('a').hover( function() {
      　　$(this).parents('tr').unbind('click');
      　}, function() {
      　　$(this).parents('tr').click( function() {
      　　　window.location = $(this).attr('data-href');
      　　});
      　});
      });

      $(function(){
        var articleId;
        $('.btn-like').addClass('clickable').click(function(){
          var $this = $(this);
          articleId = $this.parents('.post').data('postid');
          $.ajax({
            type: 'POST',
            url: "http://localhost:8888/likes/like",
            data: {postId: articleId},
            error: function(XMLHttpRequest, textStatus, errorThrown)
            {
                alert('Error : ' + 'いいねに失敗しました');
            }
          }).done(function(){
            console.log('good success');
            $this.children('i').toggleClass('far'); //空洞ハート
            $this.children('i').toggleClass('active fas'); //塗りつぶしハート
          });
        });
      });

      $(function(){
        var articleId;
        $('.btn-unlike').addClass('clickable').click(function(){
          var $this = $(this);
          articleId = $this.parents('.post').data('postid');
          $.ajax({
            type: 'POST',
            url: "http://localhost:8888/likes/unlike",
            data: {postId: articleId},
            error: function(XMLHttpRequest, textStatus, errorThrown)
            {
                alert('Error : ' + 'いいね解除に失敗しました');
            }
          }).done(function(){
            console.log('good remove success');
            $this.children('i').toggleClass('active fas'); //塗りつぶしハート
            $this.children('i').toggleClass('far'); //空洞ハート
          });
        });
      });
    </script>
</body>
</html>
