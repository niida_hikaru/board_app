<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Article $article
 */
?>

<div class="articles form large-9 medium-8 columns content">
    <?= $this->Form->create($article) ?>
    <fieldset>
        <legend><?= __('記事投稿') ?></legend>
        <?php
            echo $this->Form->control('title', ['label' => 'タイトル']);
            echo $this->Form->control('content', ['label' => '内容']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('投稿')) ?>
    <?= $this->Form->end() ?>
</div>
