<nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="<?= $this->Url->build(['controller'=>'articles', 'action'=>'index']) ?>">BoardApp</a>
      </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <?php if($auth->user()): ?>
              <li><a href="<?= $this->Url->build(['controller'=>'articles', 'action'=>'add']) ?>">記事を投稿する</a></li>
              <li><a href="<?= $this->Url->build(['controller'=>'articles', 'action'=>'index']) ?>">記事一覧</a></li>
              <li><a href="<?= $this->Url->build(['controller'=>'users', 'action'=>'mypage']) ?>">マイページ</a></li>
              <li><a href="<?= $this->Url->build(['controller'=>'users', 'action'=>'logout']) ?>">ログアウト</a></li>
          <?php else: ?>
              <li><a href="<?= $this->Url->build(['controller'=>'users', 'action'=>'login']) ?>">ログインする</a></li>
              <li><a href="<?= $this->Url->build(['controller'=>'users', 'action'=>'create']) ?>">会員登録</a></li>
          <?php endif; ?>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>