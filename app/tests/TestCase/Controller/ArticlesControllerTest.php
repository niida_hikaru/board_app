<?php
namespace App\Test\TestCase\Controller;

use App\Controller\articlesController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;


/**
 * App\Controller\articlesController Test Case
 */
class articlesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.articles',
        'app.users',
        'app.comments',
        'app.likes',
        'app.social_profiles',
        'app.forgot_passwords'
    ];

    public function setUp()
    {
        parent::setUp();
        $this->Articles = TableRegistry::getTableLocator()->get('Articles');
        $this->Likes = TableRegistry::getTableLocator()->get('Likes');
    }

    public function testIndex()
    {
        $this->get('/articles/index');
        $this->assertResponseOk();
    }

    public function testView(){
        // IDなしはエラー
        $this->get('/articles/view');
        $this->assertResponseError();

        // 存在するデータ
        $this->get('/articles/view/1');
        $this->assertResponseOk();

        // 存在しないデータはエラー
        $this->get('/topics/view/2');
        $this->assertResponseError();
    }

    //ログインしている時に記事を投稿できること
    public function testAdd(){
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'test@test.com',
                ]
            ]
        ]);

        $data = [
            'id' => 3,
            'title' => 'test',
            'content' => 'aaaaa',
            'user_id' => 1,
            'careted' => '2019-03-12 15:10:24',
            'modified' => '2019-03-12 15:22:24'
        ];

        $this->post('articles/add', $data);
        $this->assertResponseSuccess();
        $this->assertRedirect(['action'=> 'index']);

        $query = $this->Articles->find()
            ->where(['id' => 3])
            ->andWhere(['title'=> 'test'])
            ->andWhere(['content' => 'aaaaa']);

        $this->assertEquals(1, $query->count());
    }

    //ログインしていない時に記事を投稿できないこと
    public function testAddFailure(){
        $data = [
            'title' => 'test',
            'content' => 'aaaaa',
            'user_id' => '1',
            'careted' => '2019-03-12 15:10:24',
            'modified' => '2019-03-12 15:22:24'
        ];

        $this->post('articles/add', $data);
        $this->assertResponseFailure();
    }

    //ログインしている時に記事を編集できること
    public function testEdit(){
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'test@test.com',
                ]
            ]
        ]);
        $this->get('/articles/edit/1');
        $this->assertResponseOk();
        
        $data = [
            'title' => 'test edit',
            'content' => 'test edit'
        ];

        $this->patch('articles/edit/1', $data);
        $query = $this->Articles->find()
            ->where(['title' => $data['title']]);
        $this->assertResponseSuccess();
        $this->assertRedirect(['action'=> 'index']);
    }

    //ログインしていない時に記事を編集できないこと
    public function testEditFailure(){
        $data = [
            'title' => 'test edit',
            'content' => 'test edit'
        ];

        $this->patch('articles/edit/1', $data);
        $query = $this->Articles->find()
            ->where(['title' => $data['title']]);
        
        $this->assertEquals(0, $query->count());
    }

    //自分以外の投稿を編集できないこと
    public function testEditOwnArticleFailure(){
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'test@test.com',
                ]
            ]
        ]);
        
        $data = [
            'title' => 'test edit fail',
            'content' => 'test edit fail'
        ];

        $this->patch('articles/edit/2', $data);
        $query = $this->Articles->find()
            ->where(['title' => $data['title']])
            ->andWhere(['content' => 'test edit fail']);
                
        $this->assertEquals(0, $query->count());
    }

    //記事を削除できること
    public function testDelete(){
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'test@test.com',
                ]
            ]
        ]);

        $this->post('articles/delete/1');

        $query = $this->Articles->find()
            ->where(['id' => 1]);

        $this->assertEquals(0, $query->count());
    }

    //GETで記事を削除できないこと
    public function testDeleteFailure(){
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'test@test.com',
                ]
            ]
        ]);

        $this->get('articles/delete/1');

        $query = $this->Articles->find()
            ->where(['id' => 1]);

        $this->assertEquals(1, $query->count());
    }

    //記事のいいね総数が正しいこと
    public function testCountLikes(){
        $totalLikes = $this->Likes->find()
            ->where(['article_id' => 1])
            ->count();

        $this->assertEquals(2, $totalLikes);
        
    }

/*
    public function testIndexQueryData()
    {
        $this->get('/articles?page=1');

        $this->assertResponseOk();
        // 他のアサート

        $this->get('/articles?page=2');

        $this->assertResponseOk();
    }*/
}
