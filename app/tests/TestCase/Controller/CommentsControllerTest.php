<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CommentsController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\CommentsController Test Case
 */
class CommentsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.comments',
        'app.articles',
        'app.users'
    ];

    public function setUp()
    {
        parent::setUp();
        $this->Comments = TableRegistry::getTableLocator()->get('Comments');
    }

    //ログインしている時コメントが追加できること
    public function testAdd()
    {   
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'test@test.com',
                ]
            ]
        ]);

        $data = [
            'name' => 'test',
            'comment' => 'test comment',
            'article_id' => 1,
            'user_id' => 1
        ];

        $this->post('comments/add', $data);
        $this->assertResponseSuccess();

        $query = $this->Comments->find()
            ->where(['name' => 'test'])
            ->andWhere(['comment'=> 'test comment']);

        $this->assertEquals(1, $query->count());
    }

    //ログインしていない時コメントが追加できないこと
    public function testAddNotAuthenticated()
    {
        $data = [
            'name' => 'test',
            'comment' => 'test comment',
            'article_id' => 1,
            'user_id' => 1
        ];

        $this->post('comments/add', $data);
        $this->assertResponseFailure();
    }

    //自分のコメントが削除できること
    public function testDelete()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'test@test.com',
                ]
            ]
        ]);

        $this->post('comments/delete/1');
        $this->assertResponseSuccess();

        $query = $this->Comments->find()
            ->where(['name' => 'test delete'])
            ->andWhere(['comment'=> 'test comment']);

        $this->assertEquals(0, $query->count());
    }

    //他人のコメントが削除できないこと
    public function testDeleteNotOwnComment()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'test@test.com',
                ]
            ]
        ]);

        $this->post('comments/delete/2');
        $query = $this->Comments->find()
            ->where(['name' => 'test2 delete'])
            ->andWhere(['comment'=> 'test2 comment']);

        $this->assertEquals(1, $query->count());
    }
}
