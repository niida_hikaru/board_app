<?php
use Migrations\AbstractSeed;

/**
 * Likes seed.
 */
class LikesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '3',
                'user_id' => '3',
                'article_id' => '2',
                'created' => '2019-03-13 08:13:14',
                'modified' => '2019-03-13 08:14:33',
            ],
            [
                'id' => '4',
                'user_id' => '3',
                'article_id' => '3',
                'created' => '2019-03-13 08:28:09',
                'modified' => '2019-03-13 08:28:09',
            ],
            [
                'id' => '5',
                'user_id' => '3',
                'article_id' => '4',
                'created' => '2019-03-13 08:53:37',
                'modified' => '2019-03-13 12:36:22',
            ],
        ];

        $table = $this->table('likes');
        $table->insert($data)->save();
    }
}
