#!/usr/bin/env bash

if [ -n ${COMPOSER_FLAG} ]; then
    cd $APP_PATH
    composer install -o
fi

if [ -n ${MIGRATIONS_FLAG} ]; then
    cd $APP_PATH
    bin/cake migrations migrate
    bin/cake migrations seed
fi

php-fpm
