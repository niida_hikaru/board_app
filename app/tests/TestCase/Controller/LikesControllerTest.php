<?php
namespace App\Test\TestCase\Controller;

use App\Controller\LikesController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\LikesController Test Case
 */
class LikesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.likes',
        'app.users',
        'app.articles',
        'app.comments'
    ];

    public function setUp()
    {
        parent::setUp();
        $this->Likes = TableRegistry::getTableLocator()->get('Likes');
    }

    //いいねできること
    public function testLike()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    'email' => 'test2@test.com',
                ]
            ]
        ]);

        $data = [
            'postId' => 2
        ];

        //ajax偽装
        $this->configRequest([
            'headers' => [
                'X-Requested-With' => 'XMLHttpRequest'
            ],
        ]);

        $this->post('likes/like', $data);
        $this->assertResponseSuccess();
        
        $query = $this->Likes->find()
            ->where(['user_id'=> 2])
            ->andWhere(['article_id' => 2]);

        $this->assertEquals(1, $query->count());
    }

    //いいね解除できること
    public function testUnlike()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'test2@test.com',
                ]
            ]
        ]);

        $data = [
            'postId' => 1
        ];

        //ajax偽装
        $this->configRequest([
            'headers' => [
                'X-Requested-With' => 'XMLHttpRequest'
            ],
        ]);

        $this->post('likes/unlike', $data);
        $this->assertResponseSuccess();
        
        $query = $this->Likes->find()
            ->where(['user_id'=> 1])
            ->andWhere(['article_id' => 1]);

        $this->assertEquals(0, $query->count());
    }
}
