<?php
/**
 * @var \App\View\AppView $this
 */
?>
<?= $this->Flash->render('auth') ?>
<h3 class="text-center"><?= __('ログイン') ?></h3>

<div class="users form">
    <?= $this->Form->create() ?>
    <fieldset>
        <?= $this->Form->control('email', ['label' => 'メールアドレス']) ?>
        <?= $this->Form->control('password', ['label' => 'パスワード']) ?>
    </fieldset>
    <?= $this->Form->button(__('ログイン')); ?>
    <?= $this->Form->end() ?>
    <hr style="margin: 40px 0;">
    <div class="text-center">
        <u><a href="<?= $this->Url->build(['controller'=>'Users', 'action'=>'create']) ?>">新規で会員登録する</a></u>
        <p>
        <?= $this->Form->postLink(
        'Login with Google',
        ['controller' => 'Users', 'action' => 'login', '?' => ['provider' => 'Google']]
        ) ?>
        </p>
        <p>
        <?= $this->Form->postLink(
        'Login with Twitter',
        ['controller' => 'Users', 'action' => 'login', '?' => ['provider' => 'Twitter']]
        ) ?>
        </p>
        <p>
        <?= $this->Html->link(
        'パスワードを忘れた方はこちらへ',
        ['controller' => 'Users', 'action' => 'forgotPassword']
        ) ?>
        </p>
    </div>
</div>
