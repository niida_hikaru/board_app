<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<h3 class="text-center"><?= __('会員登録') ?></h3><br>

<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <?php
            echo $this->Form->control('name', ['label' => 'ユーザー名']);
            echo $this->Form->control('email', ['label' => 'メールアドレス']);
            echo $this->Form->control('password', ['label' => 'パスワード']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('登録')) ?>
    <?= $this->Form->end() ?>
</div>
