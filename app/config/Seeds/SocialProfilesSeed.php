<?php
use Migrations\AbstractSeed;

/**
 * SocialProfiles seed.
 */
class SocialProfilesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '2',
                'user_id' => '13',
                'provider' => 'Google',
                'identifier' => '117008409598834604346',
                'profile_url' => '',
                'website_url' => NULL,
                'photo_url' => 'https://lh4.googleusercontent.com/-Pm8THtE6grI/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rdmpKJCzx2oDOeKYdU8NVvfn7pc7w/mo/photo.jpg',
                'display_name' => '仁井田 輝',
                'description' => NULL,
                'first_name' => '輝',
                'last_name' => '仁井田 ',
                'gender' => '',
                'language' => 'ja',
                'age' => NULL,
                'birth_day' => NULL,
                'birth_month' => NULL,
                'birth_year' => NULL,
                'email' => 'niida_hikaru@giginc.co.jp',
                'email_verified' => 'niida_hikaru@giginc.co.jp',
                'phone' => NULL,
                'address' => NULL,
                'country' => NULL,
                'region' => NULL,
                'city' => NULL,
                'zip' => NULL,
                'created' => '2019-03-18 01:34:49',
                'modified' => '2019-03-18 01:37:49',
            ],
            [
                'id' => '4',
                'user_id' => '14',
                'provider' => 'Google',
                'identifier' => '102908589956163019569',
                'profile_url' => '',
                'website_url' => NULL,
                'photo_url' => 'https://lh4.googleusercontent.com/-mqX_pq99BBE/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rfjB0vBxe3381sh6-dzDk89tTgTKw/mo/photo.jpg',
                'display_name' => '仁井田輝',
                'description' => NULL,
                'first_name' => '輝',
                'last_name' => '仁井田',
                'gender' => '',
                'language' => 'ja',
                'age' => NULL,
                'birth_day' => NULL,
                'birth_month' => NULL,
                'birth_year' => NULL,
                'email' => 'niida.hikaru@gmail.com',
                'email_verified' => 'niida.hikaru@gmail.com',
                'phone' => NULL,
                'address' => NULL,
                'country' => NULL,
                'region' => NULL,
                'city' => NULL,
                'zip' => NULL,
                'created' => '2019-03-18 01:45:36',
                'modified' => '2019-03-18 01:45:36',
            ],
        ];

        $table = $this->table('social_profiles');
        $table->insert($data)->save();
    }
}
