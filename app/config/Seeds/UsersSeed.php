<?php
use Migrations\AbstractSeed;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '2',
                'name' => 'yamada',
                'email' => 'yamada@test.com',
                'password' => '$2y$10$WJHLQCpirjXYdWCME83k4eQJy.30ESOyYsAXh8cRocCrYad0kEWBa',
                'created' => '2019-03-11 05:32:05',
                'modified' => '2019-03-11 05:32:05',
            ],
            [
                'id' => '3',
                'name' => 'testdayo',
                'email' => 'testdayo@gmail.com',
                'password' => '$2y$10$q8GDtRWGE8l6l4mXKQ9J3./6uQtW3tWgcxKTu23RHlxUWqwavVpoi',
                'created' => '2019-03-11 06:07:07',
                'modified' => '2019-03-18 01:17:34',
            ],
            [
                'id' => '4',
                'name' => 'niida',
                'email' => 'niida@test.com',
                'password' => '$2y$10$88oX9ApHXGjMx5cYic8Vke3VqZBFQWe6qZFiaSplYfafSIwSol0I2',
                'created' => '2019-03-11 06:15:01',
                'modified' => '2019-03-11 06:15:01',
            ],
            [
                'id' => '5',
                'name' => 'ひかる',
                'email' => 'hikaru@gmail.com',
                'password' => '$2y$10$m.WDSK25Lww11UqnFvJwdeKD28sQt2v2W8mlt2.xt8uSxU2kjM3n6',
                'created' => '2019-03-12 02:43:52',
                'modified' => '2019-03-12 02:43:52',
            ],
            [
                'id' => '6',
                'name' => 'pass_test',
                'email' => 'pass@test.com',
                'password' => '$2y$10$6pVVFKFmXlk.ImPusx6SBOMTlDKdnHTYE1pZKSfAS8ba.Z6N4HIT2',
                'created' => '2019-03-12 03:12:51',
                'modified' => '2019-03-12 03:12:51',
            ],
            [
                'id' => '7',
                'name' => 'kkkk',
                'email' => 'kkk@test.com',
                'password' => '$2y$10$1w.K9yi6pI48F2jninIQQOj1yhSpmuanGEIkrjLTChEke/U4Twuu6',
                'created' => '2019-03-12 03:13:47',
                'modified' => '2019-03-12 03:13:47',
            ],
            [
                'id' => '8',
                'name' => 'ppp',
                'email' => 'ppp@test.com',
                'password' => '$2y$10$6m9feiY7scMqoTGMEQDZDeGJjLog7TxqIEwfhRFkrEyrr0Ewt8LCq',
                'created' => '2019-03-12 03:24:12',
                'modified' => '2019-03-12 03:24:12',
            ],
            [
                'id' => '9',
                'name' => 'tetet',
                'email' => 'tete@test.com',
                'password' => '$2y$10$9n8Y7C7bRLIwfgD6k7z8VurH622oAw9h50T6GXhYfGhmcb0NOOzbS',
                'created' => '2019-03-13 08:27:04',
                'modified' => '2019-03-13 08:27:04',
            ],
            [
                'id' => '10',
                'name' => 'kimura',
                'email' => 'kimu@gmai.com',
                'password' => '$2y$10$nuDsXTRw7G2G8uVE4wxB5eMIc4iAJupHaMJxHxdc2VQTgyA37bgNC',
                'created' => '2019-03-15 02:50:32',
                'modified' => '2019-03-15 02:50:32',
            ],
            [
                'id' => '11',
                'name' => 'niida',
                'email' => 'hikaru215soccer@yahoo.co.jp',
                'password' => '$2y$10$7snbv9SWT1a4iRHkAvTwL.RzmNUp80sZwEJjUHpUK2HQ/E1iSOZpG',
                'created' => '2019-03-15 03:01:19',
                'modified' => '2019-03-15 03:01:19',
            ],
            [
                'id' => '13',
                'name' => '仁井田 輝',
                'email' => 'niida_hikaru@giginc.co.jp',
                'password' => NULL,
                'created' => '2019-03-18 01:34:49',
                'modified' => '2019-03-18 01:34:49',
            ],
            [
                'id' => '14',
                'name' => '仁井田輝',
                'email' => 'niida.hikaru@gmail.com',
                'password' => NULL,
                'created' => '2019-03-18 01:45:36',
                'modified' => '2019-03-18 01:45:36',
            ],
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
