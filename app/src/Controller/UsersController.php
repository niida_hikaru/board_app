<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Mailer\Email;
use Cake\Error\Debugger;


/**
 * Users Controller
 *
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function create()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect($this->Auth->redirectUrl(['controller'=>'Users', 'action'=>'login']));
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function login(){
        if($this->request->is('post')){
            $user = $this->Auth->identify();
            if($user){
                $this->Auth->setUser($user);

                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error(__('メールアドレスまたはパスワードが不正です。'));
            }
        }
    }
    
    public function logout(){
        $this->Flash->success('ログアウトしました。');
        return $this->redirect($this->Auth->logout());
    } 

    public function forgotPassword(){
        $this->loadModel('ForgotPasswords');
        $forgot_password = $this->ForgotPasswords->newEntity();

        if($this->request->is('post')){
            $user = $this->Users->findByEmail($this->request->data['email']);
            $user = $user->first();

            if($user){
                $token = \Cake\Utility\Text::uuid();
                $url = Router::Url(['controller' => 'users', 'action' => 'resetPassword'], true) . '/' . $token;
                $timeout = time() + DAY;
                $data = [
                    'token' => $token,
                    'user_id' => $user->id,
                    'timeout' => $timeout
                ];
                $forgot_password = $this->ForgotPasswords->patchEntity($forgot_password, $data);
                if($this->ForgotPasswords->save($forgot_password)){
                    $this->sendResetEmail($url, $user);
                    $this->redirect(['action' => 'login']);
                }else{
                    $this->Flash->error('Error saving reset passkey/timeout');
                }
            }else{
                $this->Flash->error('送信頂いたメールアドレスのアカウントがありませんでした。');
            }
        }
    }


    public function mypage(){
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => ['SocialProfiles']
        ]);

        //ユーザーが持っている記事一覧取得        
        $this->loadModel('Articles');
        $ownArticles = $this->Articles->find()
            ->where(['user_id' => $this->Auth->user('id')]);
        
        //ユーザー情報変更
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('プロフィール設定を変更しました。'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('プロフィールの変更を保存できませんでした。時間を置いて試しみてください。'));
        }
        $this->set(compact('user', 'ownArticles'));
    }

    private function sendResetEmail($url, $user){
        $email = new Email();
        $email->template('resetpw');
        $email->emailFormat('html');
        $email->from('niida.hikaru@gmail.com');
        $email->to($user->email);
        $email->subject('Reset your password');
        $email->viewVars(['url' => $url, 'username' => $user->name]);
        if ($email->send()) {
            $this->Flash->success(__('Check your email for your reset password link'));
        } else {
            $this->Flash->error(__('Error sending email: ') . $email->smtpError);
        }
    }

    public function resetPassword($token = null){
        if($token){
            $query = $this->Users->find('all', [
                'conditions' => ['token' => $token, 'timeout >' => time()],
                'contain' => ['ForgotPasswords']
            ]);
            $user = $query->first();
            if($user){
                if(!empty($this->request->getData())){
                    $user = $this->Users->patchEntity($user, $this->request->getData());
                    if($this->Users->save($user)){
                        $this->loadModel('ForgotPasswords');
                        $ForgotPasswords = $this->ForgotPasswords->get($user->id);

                        if ($this->ForgotPasswords->delete($ForgotPasswords)) {
                            $this->Flash->set(__('Your password has been updated.'));
                            return $this->redirect(['action' => 'login']);
                        } else {
                            $this->Flash->error(__('The token could not be deleted. Please, try again.'));
                        }
                    } else {
                        $this->Flash->error(__('The password could not be updated. Please, try again.'));
                    }
                }
            }else{
                $this->Flash->error('Invalid or expired passkey. Please check your email or try again');
                $this->redirect(['action' => 'fogotPassword']);
            }
            $this->set(compact('user'));
        }else{
            $this->redirect('/');
        }
    }

    public function isAuthorized($user){
        return true;        
    }

}
