# This repository is CakePHP 3 sample project


## Dependence

* [PHP](https://secure.php.net/) 7.1以上
* [CakePHP](https://secure.php.net/) 3.5以上

## Get Started

### dockerをインストール

[MacOS docker](https://docs.docker.com/docker-for-mac/)


### 起動
ホームディレクトリでこちらを実行してください。
```
$ docker-compose up -d
```

### 停止
```
$ docker-compose down
```

### 開発

- サイト
	- http://localhost:8888


- phpMyAdmin
	- http://localhost:8889


- MailHog
	- http://localhost:8025



### コマンド

初期のデータベースを更新

```
$ docker exec cakephp_mysql /usr/bin/mysqldump -u root --password=password cakephp > sql/cakephp.sql
```

Dockerコンテナ一覧表示

```
$ docker ps -a
```

Dockerコンテナ一括削除

```
$ docker rm `docker ps -a -q`
```
