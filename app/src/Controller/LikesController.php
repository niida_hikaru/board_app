<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Error\Debugger;

/**
 * Likes Controller
 *
 * @property \App\Model\Table\LikesTable $Likes
 *
 * @method \App\Model\Entity\Like[] paginate($object = null, array $settings = [])
 */
class LikesController extends AppController
{
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

    public function like(){
        $this->autoRender = false;

        $data = [ 
            'article_id' => $this->request->getData('postId'),
            'user_id' => $this->Auth->user('id')
        ];
        $like = $this->Likes->newEntity();
        
        if ($this->request->is('ajax')) {
            $like = $this->Likes->patchEntity($like, $data);
            $this->Likes->save($like);
            
        }
    }

    public function unlike(){
        $this->autoRender = false;

        if ($this->request->is('ajax')){
            $this->Likes->deleteAll([
                'article_id' => $this->request->getData('postId'), 
                'user_id' => $this->Auth->user('id')
            ]);
        }
    }

    public function isAuthorized($user){
        if(in_array($this->request->getParam('action'), ['like', 'unlike'])){
            return true;
        }
        
        return parent::isAuthorized($user);
    }

}
