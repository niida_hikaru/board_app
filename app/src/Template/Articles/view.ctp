<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Article $article
 */
?>
<h3 class="text-center"><?= __('記事詳細') ?></h3><br>
<div class="container">
<h6 class="text-right">
    <?php if($article->user_id === $auth->user('id')): ?>
        <?= $this->Html->link(__(''), ['action' => 'edit', $article->id],
            ['class' => 'fa-edit fa-2x fas']) ?>
        <?= $this->Form->postLink(__(''), ['action' => 'delete', $article->id], ['class' => 'fa-trash fa-2x fas'], ['confirm' => __('Are you sure you want to delete # {0}?', $article->id)]) ?>
    <?php endif; ?>
    <span class="post" data-postid="<?= $article->id ?>">
    <?php 
        if($like === 0){
            $out = '<span class="btn-like">';
            $out .= '<i class="fa-heart fa-2x far"></i>';
            $out .= "</span>";
            echo $out;
        }else{
            $out = '<span class="btn-unlike">';
            $out .= '<i class="fa-heart fa-2x active fas"></i>';
            $out .= "</span>";
            echo $out;
        }
    ?>
    </span>
</h6>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3><?= h($article->title) ?></h3>
        </div>
        <div class="panel-body">
            <?= $this->Text->autoParagraph(h($article->content)) ?>
            <?php if($article->user->social_profiles): ?>
                <p class="text-right font-weight-bold mr-10">投稿者: <?= $article->user['social_profiles'][0]['display_name'] ?></p>
            <?php else: ?>
                <p class="text-right font-weight-bold mr-10">投稿者: <?= $article->user->name ?></p>
            <?php endif; ?>
        </div>
    </div>
    <?php if($article->comments): ?>
    <legend>コメント</legend>
    <div class="panel panel-default">
        <div class="list-group">
            <?php foreach ($article->comments as $comment): ?>
            <div class="list-group-item">
                <h5><?= h($comment->comment) ?></h5>
                <h6 class="text-right font-weight-bold mr-10">
                    <?= h($comment->name) ?>
                    <?= $comment->created->i18nFormat('YYYY/MM/dd HH:mm:ss') ?>
                    <?php if($comment->user_id === $auth->user('id')): ?>
                        <?= $this->Form->postLink(__('削除'), ['controller' => 'comments', 'action' => 'delete', $comment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $comment->id)]) ?>
                    <?php endif; ?>
                </h6>
            </div>        
            <?php endforeach; ?>
        </div>
    </div>
    <?php endif; ?>
    <br>
    <?= $this->Form->create(null, [
        'url' => ['controller' => 'Comments', 'action' => 'add']
    ]) ?>
        <?php
            echo $this->Form->control('name', ['label' => '名前']);
            echo $this->Form->control('comment', ['label' => 'コメント']);
            echo $this->Form->hidden('article_id', ['value' => $article->id]);
            echo $this->Form->hidden('user_id', ['value' => $auth->user('id')]);
        ?>
    <?= $this->Form->button(__('コメントを送信')) ?>
    <?= $this->Form->end() ?>
</div><br>