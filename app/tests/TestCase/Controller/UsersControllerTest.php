<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UsersController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\TestSuite;
use Cake\ORM\TableRegistry;
use Cake\I18n\FrozenTime;

/**
 * App\Controller\UsersController Test Case
 */
class UsersControllerTest extends IntegrationTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users'
    ];

    public function setUp()
    {
        parent::setUp();
        $this->Users = TableRegistry::getTableLocator()->get('Users');
        $forgotPassword = TableRegistry::getTableLocator()->get('ForgotPasswords');        
    }


    public function testCreate()
    {
        $data = [
            'name' => 'yamada',
            'email' => 'yamada@test.com',
            'password' => 'test1234',
            'careted' => '2019-03-11 15:10:24',
            'modified' => '2019-03-11 15:22:24'
        ];

        $this->post('users/create', $data);
        $this->assertResponseSuccess();
        $this->assertRedirect(['action'=> 'login']);

        $query = $this->Users->find()
            ->where(['email'=> $data['email']]);
        $this->assertEquals(1, $query->count());
    }

    public function testChangeUserInfoOnMypage(){
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'test@test.com',
                ]
            ]
        ]);

        $this->get('users/mypage');
        $this->assertRedirect(['action'=> 'mypage']);
        //$this->assertResponseSuccess();


    }

    //ログインできること
    /* やり方調査中
    public function testLogin()
    {
        $data = [
            'email' => 'test@test.com',
            'password' => 'test1234'
        ];

        $this->post('/users/login', $data);
        $this->assertResponseSuccess();
    
        $user = [
            'id' => 1,
            'name' => 'Alex',
            'email' => 'test@test.com',
            'created' => new FrozenTime('2019-03-04 02:08:46'),
            'modified' => new FrozenTime('2019-03-04 02:08:46')
        ];

        $this->assertSession(1, 'Auth.User.id');
    }*/

    //resetメールが送信されること
    //エラー調査中Error: Call to undefined method App\Test\TestCase\Controller\UsersControllerTest::assertEmailFrom()
    /*
    public function testForgotPassword(){
        $data = [
            'email' => 'niida.hikaru@gmail.com'
        ];
        $from = 'niida.hikaru@gmail.com';
        $to = 'test@test.com';

        $this->post('users/forgotPassword', $data);
        $this->assertResponseSuccess();

        $this->assertEmailFrom($from);
        //$this->assertEmailTo($to);
    }*/

}
