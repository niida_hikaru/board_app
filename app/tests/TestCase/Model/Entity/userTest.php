<?php
namespace App\Test\TestCase\Model\Entity;

use App\Model\Entity\user;
use Cake\TestSuite\TestCase;
use Cake\Auth\DefaultPasswordHasher;

/**
 * App\Model\Entity\user Test Case
 */
class userTest extends TestCase
{
    //ハッシュ化パスワードを保存する場合
    public function testSetPassword(){
        $user = new User();

        $rawPass = 'secret';
        $user->password = $rawPass;
        $hashPass = $user->password;

        $this->assertNotSame($rawPass, $hashPass);

        $hasher = new DefaultPasswordHasher();
        $this->assertTrue($hasher->check($rawPass, $hashPass));
    }
}
