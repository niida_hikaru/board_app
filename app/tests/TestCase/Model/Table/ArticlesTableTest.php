<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\articlesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\articlesTable Test Case
 */
class articlesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\articlesTable
     */
    public $articles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.articles',
        'app.users',
        'app.articles',
        'app.comments',
        'app.likes',
        'app.social_profiles',
        'app.forgot_passwords'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('articles') ? [] : ['className' => articlesTable::class];
        $this->Articles = TableRegistry::get('articles', $config);
    }

    public function testFindArticle(){
        $query = $this->Articles->find('all')
            ->select(['id', 'title', 'content', 'user_id']);
        $this->assertInstanceOf('Cake\ORM\Query', $query);
        $result = $query->enableHydration(false)->toArray();

        $expected = [
            [
                'id' => 1,
                'title' => 'test article',
                'content' => 'content test', 
                'user_id' => 1
            ]
        ];
        $this->assertEquals($expected, $result);
    }

     /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        //エラー無しの場合
        $article = $this->Articles->newEntity([
            'title' => 'test1 title',
            'content' => 'test content'
        ]);
        $expected = [];
        $this->assertSame($expected, $article->getErrors());

        //必須項目が空の場合
        $article = $user = $this->Articles->newEntity([
            'title' => '',
            'content' => ''
        ]);
        $expected = [
            'title' => ['_empty' => 'This field cannot be left empty'],
            'content' => ['_empty' => 'This field cannot be left empty'],
        ];
        $this->assertSame($expected, $article->getErrors());

        //文字数が多い場合 (title)
        $article = $this->Articles->newEntity([
            'title' => str_repeat('a', 51),
        ]);
        $expected = [
            'title' => ['maxLength' => '50字以内で入力してください'],
        ];
        $this->assertSame($expected, $article->getErrors());
    }
}
